/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include "bootflags.h"
#include "ecc_tables.h"

/* Return the other record that contains the same type of data as X.
 * SIBLING(0) == 1, SIBLING(1) == 0 (the other mini-userspace)
 * SIBLING(2) == 3, SIBLING(3) == 2 (the other full-userspace)
 */
#define SIBLING(X) ( ((X+1) % 2) + 2*(X/2) )

static enum flaglib_mode mode;

/* when running as a u-boot standalone app we don't get memcpy... */
static void *mycpy(void *dst, const void *src, unsigned int len)
{
	char *ret = dst;

	while (len-- > 0) {
		*ret++ = *((char *)src);
		src++;
        }
        return ret;
}

/*
  Simple systemic hamming(8,4) code
  data and parity are stored separately and recombined for tests
  In real failure situations it's far more likely that entire blocks will
  be corrupted, so a fast simple check is used here instead of something
  more elaborate.

  The codes have been xored with an arbitrary value to ensure that an
  all 0 block won't appear valid
*/

static void set_ecc(struct flag_entry *in)
{
	int parts, i, nibble;
	unsigned char *ptr;
	for (parts = 0; parts < 4; parts++) {
		ptr = (unsigned char *)(in + parts);
		for (i = 0; i < FLAG_BYTES * 2; i++) {
			nibble = ptr[i/2];
			if (!(i % 2)) {
				in[parts].ecc[i/2] = 0;
				nibble >>= 4;
			} else
				nibble &= 0xF;
			in[parts].ecc[i/2] |= (ecc_enc_table[nibble] >> 4)
			                      << (4 * (i%2));
		}
	}
}

/* so a test app can create valid ECC.. */
void flaglib_set_ecc_for_testing(struct flag_entry *in)
{
	set_ecc(in);
}
/* tests the ECC data and corrects any data errors
   returns number of errors or -1 if an uncorrectable error occurred
   NOTE: only data errors are corrected, ECC should be recalculated if
   the return is > 0
*/
int flaglib_check_ecc(struct flag_entry *in, unsigned int n_parts)
{
	int code, i, data, errcount = 0;
	unsigned int parts;
	unsigned char *ptr;

	for (parts = 0; parts < n_parts; parts++) {
		ptr = (unsigned char *)(in + parts);
		for (i = 0; i < FLAG_BYTES * 2; i++) {
			data = ptr[i/2];
			code = in[parts].ecc[i/2];
			if (!(i % 2)) {
				code &= 0xF;
				code <<= 4;
				data >>= 4;
				code |= data;
			} else {
				code &= 0xF0;
				data &= 0xF;
				code |= data;
			}
			if (ecc_dec_table[code] == 0xFFFF)
				return -1;
			if (ecc_enc_table[data] != code) {
				int correct = ecc_dec_table[code];
				errcount++;
				data = ptr[i/2];
				if (!(i % 2)) {
					data &= 0xF;
					data |= correct << 4;
				} else {
					data &= 0xF0;
					data |= correct;
				}
				ptr[i/2] = data;
			}
		}
	}
	return errcount;
}

int test_dec(struct flag_entry *in)
{
	int x;

	x = flaglib_check_ecc(in, 4);
	return x;
}

/* Set the flags in such a way that a factory reset will be forced
 */
static void force_factory_reset(struct flag_entry *in)
{
	int i;
	/* setting it all failed is a start, but we need to make sure we
	   pass all the sanity checks too */
	for (i = 0; i < 4; i++) {
		in[i].flags = FLAG_FAILED | FLAG_IN_USE;
		/* version needs to be anything but 0 */
		in[i].mini_version = 1;
	}
	/* only one preferred per pair */
	in[0].flags |= FLAG_PREFERRED;
	in[2].flags |= FLAG_PREFERRED;
}

/* is the device in a fresh factory state?
   returns 1 if yes, 0 if no
*/
static int is_factory(struct flag_entry *in)
{
	int i;

	for (i = 0; i < 4; i++)
		if (!(in[i].flags & FLAG_FACTORY)) {
			return 0;
		}
	return 1;
}


/* Some basic tests to make sure the data isn't corrupt or inconsistent
   returns 1 for error, else 0
*/
int flaglib_sanity_check(struct flag_entry *in)
{
	int i, running, starting;

	/* any in use partition must have a non zero version */
	for (i = 0; i < 4; i++)
		if ((in[i].flags & FLAG_IN_USE) && (in[i].mini_version == 0))
			return 1;

	/* Must have one and only one preferred partition from each pair*/
	if (((in[0].flags & FLAG_PREFERRED) == (in[1].flags & FLAG_PREFERRED))
	 || ((in[2].flags & FLAG_PREFERRED) == (in[3].flags & FLAG_PREFERRED)))
		return 1;

	/* can't have more than 1 partition in the starting state or 2
	 *  running
	 */
	for (i = 0, running = 0, starting = 0; i < 4; i++) {
		running += !!(in[i].flags & FLAG_RUNNING);
		starting += !!(in[i].flags & FLAG_STARTING);
	}
	if (running > 2 || starting > 1)
		return 1;

	/* can't have two of the same type running */
	if (((in[0].flags & FLAG_RUNNING) && (in[1].flags & FLAG_RUNNING))
	 || ((in[2].flags & FLAG_RUNNING) && (in[3].flags & FLAG_RUNNING)))
		return 1;

	/* Must have at least 1 in use partition of each type */
	if (! ((in[0].flags & FLAG_IN_USE || in[1].flags & FLAG_IN_USE) &&
	       (in[2].flags & FLAG_IN_USE || in[3].flags & FLAG_IN_USE)))
		return 1;

	/* any partition UPDATING must not have any other flags */
	for (i = 0; i < 4; i++)
		if ((in[i].flags & FLAG_UPDATING)
		 && (in[i].flags != FLAG_UPDATING))
			return 1;

	return 0;
}

/* Returns 1 if any partition combination (even non preferred) can boot */
int flaglib_boot_viable(struct flag_entry *in)
{
	int mini, full;

	for (mini = 0; mini < 2; mini++) {
		if (!(in[mini].flags & FLAG_IN_USE))
			continue;
		if (in[mini].flags & FLAG_FAILED)
			continue;
		for (full = 2; full < 4; full++) {
			if (!(in[full].flags & FLAG_IN_USE))
				continue;
			if (in[full].flags & FLAG_FAILED)
				continue;
			if (in[full].mini_version == in[mini].mini_version)
				return 1;
		}
	}
	return 0;
}

static enum flaglib_step do_bootloader(struct flag_entry *in,
                                       enum flaglib_status s)
{
	int i, mini, full;

	if (s != FLAGLIB_SUCCEED) {
		/* watchdog timer on last boot, we use that as an indication
		 * of kernel failure, so flag the running mini as failed
		 * and reset all the full userspaces
		 */
		for (mini = 0; mini < 2; mini++)
			if ( (in[mini].flags & FLAG_RUNNING)
			  || (in[mini].flags & FLAG_STARTING)) {
				in[mini].flags &= ~FLAG_RUNNING;
				in[mini].flags &= ~FLAG_STARTING;
				in[mini].flags |= FLAG_FAILED;
				break;
			}
		/*
		if (mini == 2) then watchdog fired in u-boot...
		that doesn't make sense, so we'll assume everything is ok...
		*/
	}

	/* reset any running/starting flags first
	 *  NOTE: We can't distinguish between a power failure and a
	 *        clean shutdown...
	 */
	for (i = 0; i < 4; i++)
		in[i].flags &= ~(FLAG_STARTING | FLAG_RUNNING);

	if (!flaglib_boot_viable(in)) {
		/* that's bad so let's try a factory reset */
		if (!is_factory(in)) {
			force_factory_reset(in);
			return FLAGLIB_BOOT_FACTORY_RESET;
		}
		/* factory image but not bootable, let's see if an
		 * unfailed mini userspace exists so we can present
		 * a nice failure screen
		 */
		for (mini = 0; mini < 2; mini++) {
			if (in[mini].flags & FLAG_FAILED)
				continue;
			if (!(in[mini].flags & FLAG_IN_USE))
				continue;
			in[mini].flags |= FLAG_STARTING;
			return FLAGLIB_BOOT_PARTITION + mini;
		}
		/* it's a factory image but nothing's bootable
		 * request servicing
		 */
		return FLAGLIB_REQUEST_SERVICING;
	}
	/* we know we can boot something, let's see if it's a preferred pair */
	for (mini = -1, full = -1, i = 0; i < 2; i++) {
		if (in[i].flags & FLAG_PREFERRED)
			mini = i;
		if (in[i + 2].flags & FLAG_PREFERRED)
			full = i + 2;
	}

	if (!(in[mini].flags & FLAG_FAILED)
	    && !(in[full].flags & FLAG_FAILED)
	    && (in[mini].mini_version == in[full].mini_version)) {
		in[mini].flags |= FLAG_STARTING;
		return FLAGLIB_BOOT_PARTITION + mini;
	}
	/* we know something can boot, but it wasn't the preferred pair */
	for (mini = 0; mini < 2; mini++)
		for (full = 0; full < 2; full++) {
			if (!(in[mini].flags & FLAG_IN_USE))
				continue;
			if (!(in[full].flags & FLAG_IN_USE))
				continue;
			if (in[mini].flags & FLAG_FAILED)
				continue;
			if (in[full].flags & FLAG_FAILED)
				continue;
			if (in[mini].mini_version != in[full].mini_version)
				continue;
			in[mini].flags |= FLAG_STARTING;
			return FLAGLIB_BOOT_PARTITION + mini;
		}
	/* this should be unreachable... */
	return FLAGLIB_REQUEST_SERVICING;
}

static enum flaglib_step do_mini(struct flag_entry *in, enum flaglib_status s)
{
	int mini, full, maybe = 0;

	if (s != FLAGLIB_SUCCEED) {
		/* fail here can only mean we've failed to mount the
		 * full userspace subvolume, so flag it failed
		 */
		for (full = 2; full < 4; full++)
			if (in[full].flags & FLAG_STARTING) {
				in[full].flags ^= FLAG_STARTING;
				in[full].flags |= FLAG_FAILED;
				in[SIBLING(full)].flags |= FLAG_FROM_ROLLBACK;
				break;
			}
		if (full == 4) /* nothing was starting?  wrong. */
			return FLAGLIB_FLAGS_INCONSISTENT;

		/* now we can set the running mini partition's flag back to
		 * STARTING and try the whole procedure again... */
		for (mini = 0; mini < 2; mini++)
			if (in[mini].flags & FLAG_RUNNING) {
				in[mini].flags ^= FLAG_RUNNING;
				in[mini].flags |= FLAG_STARTING;
				break;
			}
		if (mini == 2) /* not running?  not possible... */
			return FLAGLIB_FLAGS_INCONSISTENT;
	}

	/* mark the booting partition as started */
	for (mini = 0; mini < 2; mini++)
		if (in[mini].flags & FLAG_STARTING) {
			in[mini].flags ^= FLAG_STARTING;
			in[mini].flags |= FLAG_RUNNING;
			break;
		}

	/* no partition starting?  full userspace dropped back to us
	 * so find the running partition.
	 */
	if (mini == 2)
		for (mini = 0; mini < 2; mini++)
			if (in[mini].flags & FLAG_RUNNING)
				break;

	/* not running, not starting, not good */
	if (mini == 2)
		return FLAGLIB_FLAGS_INCONSISTENT;

	/* let's see if any subvolumes will work with this */
	for (full = 2; full < 4; full++) {
		if (!(in[full].flags & FLAG_IN_USE))
			continue;
		if (in[full].flags & FLAG_FAILED)
			continue;
		if (in[full].mini_version != in[mini].mini_version)
			continue;
		maybe = full; /* in case preferred doesn't match mini_version */
		if (in[full].flags & FLAG_PREFERRED)
			break;
	}

	if ((full != 4) && (in[full].flags & FLAG_PREFERRED)) {
		/* ideal, preferred is a version match.  use it. */
		in[full].flags |= FLAG_STARTING;
		return FLAGLIB_MOUNT_SUBVOLUME_1 + full - 2;
	}
	/* try to fallback to something non-preferred */
	if (maybe) {
		in[maybe].flags |= FLAG_STARTING;
		return FLAGLIB_MOUNT_SUBVOLUME + maybe - 2;
	}
	/* no good options... see if flagging this partition as failed
	   and rebooting has potential to save us...
	*/
	in[mini].flags ^= FLAG_FAILED;
	if (flaglib_boot_viable(in))
		return FLAGLIB_REBOOT;

	/* revert the change since it didn't help */
	in[mini].flags ^= FLAG_FAILED;

	/* factory state but still broken? really bad. */
	if (is_factory(in))
		return FLAGLIB_LIMITED_FUNCTIONALITY;
	/* ok, only chance now is a factory reset, but we're in the mini
	   userspace, and need to reboot to get the boot loader to go there */
	force_factory_reset(in);
	return FLAGLIB_REBOOT;
}

static enum flaglib_step do_full(struct flag_entry *in, enum flaglib_status s)
{
	int full, mini, i;

	if (s != FLAGLIB_SUCCEED) {
		/* either we failed to start or we failed while running
		 * either way reset starting/running and flag something
		 * failed */
		for (full = 2; full < 4; full++)
			if ((in[full].flags & FLAG_RUNNING)
			 || (in[full].flags & FLAG_STARTING)) {
				in[SIBLING(full)].flags |= FLAG_FROM_ROLLBACK;
				in[full].flags &= ~(FLAG_RUNNING | FLAG_STARTING);
				in[full].flags |= FLAG_FAILED;
				if (s != FLAGLIB_BLACKLIST)
					return FLAGLIB_RETURN_TO_MINI;
				in[full].flags |= FLAG_BLACKLISTED;
				/* if both fulls use the same mini, it's not to be blacklisted. */
				if ((in[2].flags & FLAG_IN_USE) && (in[3].flags & FLAG_IN_USE)
				    && (in[2].mini_version == in[3].mini_version))
					return FLAGLIB_RETURN_TO_MINI;
				for (mini = 0; mini < 2; mini++)
					if ((in[mini].flags & FLAG_IN_USE)
					    && (in[mini].mini_version == in[full].mini_version))
						in[mini].flags |= FLAG_BLACKLISTED | FLAG_FAILED;
				return FLAGLIB_REBOOT;
			}
		/* something should've been running or starting */
		if (full == 4)
			return FLAGLIB_FLAGS_INCONSISTENT;
	}
	/* success: promote starting to running
	 * clear factory bit too.  Also reset all "clean" and
	 * "from rollback" bits.
	 */
	for (i = 0; i < 4; i++)
		in[i].flags &= ~(FLAG_CLEAN | FLAG_FROM_ROLLBACK);

	for (full = 2; full < 4; full++)
		if (in[full].flags & FLAG_STARTING) {
			in[full].flags ^= FLAG_STARTING;
			in[full].flags |= FLAG_RUNNING;
			in[full].flags &= ~FLAG_FACTORY;
			return FLAGLIB_CONTINUE;
		}

	/* if something's running perhaps we just continue */
	for (full = 2; full < 4; full++)
		if (in[full].flags & FLAG_RUNNING)
			return FLAGLIB_CONTINUE;

	/* something should've been starting or running*/
	return FLAGLIB_FLAGS_INCONSISTENT;
}

void flaglib_set_mode(enum flaglib_mode m)
{
	mode = m;
}

/* Calculate the next state to move to based on the flags data
   if called from u-boot, fail sholud be the watchdog register
   otherwise, fail is set if attempting to perform a requested action
   from the last call is unsuccessful (eg: mini-userspace can't mount
   a subvolume)
   or if full userspace is being brought down due to inconsistency
*/
enum flaglib_step flaglib_next_step(struct flag_entry *in,
                                    enum flaglib_status s)
{
	int err;
	enum flaglib_step ret;

	err = flaglib_check_ecc(in, 4);
	if (err < 0)
		return FLAGLIB_ECC_FAIL;
	/* if (err) then data is now correct but ECC is wrong
	   ecc will be corrected at the end of this function.
	*/

	if (flaglib_sanity_check(in))
		return FLAGLIB_FLAGS_INCONSISTENT;

	switch (mode) {
	case FLAGLIB_BOOTLOADER:
		ret = do_bootloader(in, s);
		break;
	case FLAGLIB_MINI_USERSPACE:
		ret = do_mini(in, s);
		break;
	case FLAGLIB_FULL_USERSPACE:
		ret = do_full(in, s);
		break;
	case FLAGLIB_UNSET:
	default:
		ret = FLAGLIB_BOOT_FACTORY_RESET;
	}
	set_ecc(in);
	return ret;
}

/*
 * Set the flags up for a factory image
 * version should be the API level of system
 */
void flaglib_factory_flags(struct flag_entry *in, uint32_t version)
{
	in[0].mini_version = version;
	in[0].flags = FLAG_IN_USE | FLAG_PREFERRED | FLAG_FACTORY | FLAG_CLEAN;
	in[1].mini_version = 0;
	in[1].flags = FLAG_FACTORY | FLAG_CLEAN;
	in[2].mini_version = version;
	in[2].flags = FLAG_IN_USE | FLAG_PREFERRED | FLAG_FACTORY | FLAG_CLEAN;
	in[3].mini_version = 0;
	in[3].flags = FLAG_FACTORY | FLAG_CLEAN;
	set_ecc(in);
}

void flaglib_fail_all(struct flag_entry *in)
{
	int i;

	for (i = 0; i < 4; i++)
		in[i].flags |= FLAG_FAILED;
	set_ecc(in);
}

/* Replace the flags with data that will cause a factory reset on next boot
*/
void flaglib_force_factory_reset(struct flag_entry *in)
{
	force_factory_reset(in);
	set_ecc(in);
}

/* start updating a mini userspace partition
 * sets flags and reports which partition should be used for update
 * return -1 means failure, 0 is the first partition, 1 is the second
 *
 * Passing a mini_version of 0 will re-use the current mini_version
 */
int flaglib_begin_mini_update(struct flag_entry *in, uint32_t mini_version)
{
	int i, err;

	err = flaglib_check_ecc(in, 4);
	if (err < 0)
		return -1;

	if (mode != FLAGLIB_MINI_USERSPACE && mode != FLAGLIB_FULL_USERSPACE)
		return -1;

	for (i = 0; i < 2; i++)
		if (!(in[i].flags & FLAG_RUNNING) && !(in[i].flags & FLAG_BLACKLISTED)) {
			in[i].flags = FLAG_UPDATING;
			in[i].mini_version = mini_version?mini_version:in[SIBLING(i)].mini_version;
			set_ecc(in);
			return i;
		}
	return -1;
}

/* start updating a full userspace subvolume
 * sets flags and reports which partition should be used for update
 * return -1 means failure, 0 is the first partition, 1 is the second
 * either we must be performing a mini update as well, or we must
 * be using the same mini_version as the running+preferred partition
 *
 * Passing a mini_version of 0 will re-use the current mini_version
 */
int flaglib_begin_full_update(struct flag_entry *in, uint32_t mini_version)
{
	uint32_t mv;
	int full, mini, err;

	err = flaglib_check_ecc(in, 4);
	if (err < 0)
		return -1;

	if (mode == FLAGLIB_FULL_USERSPACE) {
		/* if we're in full userspace we can use the !running subvolume
		 * Unless it's clean, which means it was upgraded this boot
		 * so we'll just refuse - a reboot is required for another upgrade
		 */

		for (full = 2; full < 4; full++)
			if (!(in[full].flags & FLAG_RUNNING)
			 && !(in[full].flags & FLAG_CLEAN)
			 && !(in[full].flags & FLAG_BLACKLISTED)) {
				in[full].flags = FLAG_UPDATING;
				in[full].mini_version = mini_version?mini_version:in[SIBLING(full)].mini_version;
				set_ecc(in);
				return full - 2;
			}
		return -1;
	}
	if (mode != FLAGLIB_MINI_USERSPACE)
		return -1;

	/* updates from mini userspace can make no assumption about mini_versions
	 * disallow the "0 means it stayed the same" shorthand here */
	if (mini_version == 0)
		return -1;

	/* we're doing an update from mini userspace, so must make sure
	 * we still have a bootable configuration afterwards
	 * First, any failed subvolume is ok to re-use.
	 */
	for (full = 2; full < 4; full++)
		if (in[full].flags & FLAG_FAILED) {
			in[full].flags = FLAG_UPDATING;
			in[full].mini_version = mini_version;
			set_ecc(in);
			return full - 2;
		}

	/* get the mini_version of the running mini userspace */
	for (mv = 0, mini = 0; mini < 2; mini++)
		if (in[mini].flags & FLAG_RUNNING) {
			mv = in[mini].mini_version;
			break;
		}

	if (!mv)
		return -1;

	/* see if there's an unbootable full userspace partition */
	for (full = 2; full < 4; full++)
		if (in[full].mini_version != mv) {
			in[full].flags = FLAG_UPDATING;
			in[full].mini_version = mini_version;
			set_ecc(in);
			return full - 2;
		}

	/* so both full userspace subvolumes can be booted, neither are failed
	 * just take the non preferred one
	 */
	for (full = 2; full < 4; full++)
		if (!(in[full].flags & FLAG_PREFERRED)) {
			in[full].flags = FLAG_UPDATING;
			in[full].mini_version = mini_version;
			set_ecc(in);
			return full - 2;
		}
	return -1;
}

/* signal that either a mini or full update has completed
 * if fail is set the updating partitions will be flagged unused
 * returns 0 on failure, mini_version of the udpate on success
 */
int flaglib_finish_update(struct flag_entry *in,
                          struct flag_entry *backup,
                          enum flaglib_status s)
{
	int i, err, ret_version = -1;

	err = flaglib_check_ecc(in, 4);
	if (err < 0)
		return -1;

	/* make sure something was actually updating */
	for (i = 0; i < 4; i++)
		if (in[i].flags == FLAG_UPDATING)
			break;
	if (i == 4)
		return -1;

	if (s != FLAGLIB_SUCCEED) {
		for (i = 0; i < 4; i++)
			if (in[i].flags == FLAG_UPDATING)
				in[i].flags = 0;
		set_ecc(in);
		return 0;
	}

	for (i = 0; i < 4; i++)
		if (in[i].flags == FLAG_UPDATING) {
			in[i].flags = FLAG_IN_USE | FLAG_PREFERRED | FLAG_CLEAN;
			in[SIBLING(i)].flags &= ~FLAG_PREFERRED;
			ret_version = in[i].mini_version;
		}
	mycpy(backup, in, FLAGS_SIZE);
	for (i = 0; i < 4; i++) {
		backup[i].flags &= ~(FLAG_RUNNING | FLAG_STARTING);
	}
	set_ecc(in);
	set_ecc(backup);
	return ret_version;
}

/* Returns a string corresponding to an entry in the boot_type enum */
const char *flaglib_boot_type_as_string(enum flaglib_boot_type type)
{
	switch (type) {
	case FLAGLIB_BOOT_NORMAL:
		return "NORMAL";
	case FLAGLIB_BOOT_SUCCESSFUL_UPDATE:
		return "SUCCESSFUL_UPDATE";
	case FLAGLIB_BOOT_FAILED_UPDATE:
		return "FAILED_UPDATE";
	case FLAGLIB_BOOT_ROLLBACK:
		return "ROLLBACK";
	default:
		return "UNKNOWN_BOOT_TYPE";
	}
}

/* Returns a string corresponding to an entry in the step enum */
const char *flaglib_step_as_string(int step)
{
	switch (step) {
	case FLAGLIB_CONTINUE:
		return "CONTINUE";
	case FLAGLIB_BOOT_PARTITION_1:
		return "BOOT_PARTITION_1";
	case FLAGLIB_BOOT_PARTITION_2:
		return "BOOT_PARTITION_2";
	case FLAGLIB_BOOT_FACTORY_RESET:
		return "BOOT_FACTORY_RESET";
	case FLAGLIB_MOUNT_SUBVOLUME_1:
		return "MOUNT_SUBVOLUME_1";
	case FLAGLIB_MOUNT_SUBVOLUME_2:
		return "MOUNT_SUBVOLUME_2";
	case FLAGLIB_RETURN_TO_MINI:
		return "RETURN_TO_MINI_USERSPACE";
	case FLAGLIB_REBOOT:
		return "REBOOT";
	case FLAGLIB_LIMITED_FUNCTIONALITY:
		return "LIMITED_FUNCTIONALITY";
	case FLAGLIB_REQUEST_SERVICING:
		return "REQUEST_SERVICE_SCREEN";
	case FLAGLIB_ECC_FAIL:
		return "ECC_DECODE_FAILURE";
	case FLAGLIB_FLAGS_INCONSISTENT:
		return "FLAG_INCONSISTENCY";
	default:
		return "UNKNOWN_FAILURE";
	}
}

/* Returns the mini_version for the specific entry, or -1 if the flags
 * are damaged, or if the partition/subvolume is unbootable.
 */
int flaglib_get_mini_version(struct flag_entry *in, int entry)
{
	int err;
	err = flaglib_check_ecc(in, 4);
	if (err < 0)
		return -1;
	if  (!(in[entry].flags & FLAG_IN_USE))
		return -1;
	if  (in[entry].flags & FLAG_FAILED)
		return -1;
	if  (in[entry].flags & FLAG_UPDATING)
		return -1;
	return in[entry].mini_version;
}

/* Returns a version number higher than the version of any current flag entry
 * -1 is returned on error
 */
int flaglib_get_next_version(struct flag_entry *in)
{
	int i, nv = 0, x;

	for (i = 0; i < 4; i++) {
		x = flaglib_get_mini_version(in, i);
		if (x > nv)
			nv = x;
	}
	if (nv > 0)
		return nv + 1;
	return -1;
}

/*
 * If any partition or subvolume is blacklisted, return its entry
 * mini_version is set to the version of the returned entry
 * -1 is returned if no partition is blacklisted
 * -2 is returned on error
 */
int flaglib_clear_blacklist(struct flag_entry *in, int *mini_version)
{
	int i;
	if (flaglib_check_ecc(in, 4) < 0)
		return -2;

	for (i = 0; i < 4; i++)
		if (in[i].flags & FLAG_BLACKLISTED) {
			*mini_version = in[i].mini_version;
			in[i].flags = 0;
			set_ecc(in);
			return i;
		}
	return -1;
}

/* determine from the flags what "kind" of boot this is.
   Instead of treating factory state as a special case, it will
   be treated as a "successful update" so the first time boot
*/
enum flaglib_boot_type flaglib_get_boot_type(struct flag_entry *in)
{
	int starting;
	for (starting = 2; starting < 4; starting++)
		if (in[starting].flags & FLAG_STARTING)
			break;
	/* if starting == 4 here, something's dramatically wrong
	   this is being ignored as impossible for now */

	if (in[starting].flags & FLAG_CLEAN)
		return FLAGLIB_BOOT_SUCCESSFUL_UPDATE;
	if (in[starting].flags & FLAG_FROM_ROLLBACK) {
		if (in[SIBLING(starting)].flags & FLAG_CLEAN)
			return FLAGLIB_BOOT_FAILED_UPDATE;
		else if (in[starting].flags & FLAG_BLACKLISTED)
			return FLAGLIB_BOOT_ROLLBACK;
	}
	return FLAGLIB_BOOT_NORMAL;
}

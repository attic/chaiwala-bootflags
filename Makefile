# Copyright © 2012-2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

INSTALL ?= install
PREFIX ?= $(DESTDIR)/usr
TOOLDIR ?= $(PREFIX)/sbin
HOOKDIR ?= $(PREFIX)/share/initramfs-tools/hooks
SCRIPTDIR ?= $(PREFIX)/share/initramfs-tools/scripts
INCLUDEDIR  ?= $(PREFIX)/include/bootflags
LIBDIR ?= $(PREFIX)/lib/bootflags/
SYSTEMDDIR ?= $(DESTDIR)/usr/lib/systemd
BOOTDIR ?= /boot
CC=gcc
AR=ar
KLCC=klcc
CFLAGS=-Wall -O2
CFLAGS_SA=-msoft-float -ffreestanding -ffixed-r8 -fno-common -marm -march=armv7-a -mno-thumb-interwork -mabi=aapcs-linux
PLATFORM_LIBGCC := -L $(shell dirname `$(CROSS_COMPILE)$(CC) $(CFLAGS) -print-libgcc-file-name`) -lgcc
ARCH := $(shell uname -m)
ifeq ($(ARCH),armv7l)
	CROSS_COMPILE =
all: tester flagtool check check_ecc libbootflags.a libbootflags-uboot.a flag.blob
else
	CROSS_COMPILE ?= arm-linux-gnueabihf-
all: tester flagtool check check_ecc libbootflags.a flag.blob
endif


check_ecc: ecc-tester
	./ecc-tester

check: tester tests/helper
	./tester
	PATH="$(shell pwd):$(shell pwd)/tests:$${PATH}" \
		tests/in-tempdir \
		$(shell pwd)/tests/upgrade-rollback \
		--with-flags-in-bootloader
	PATH="$(shell pwd):$(shell pwd)/tests:$${PATH}" \
		tests/in-tempdir \
		$(shell pwd)/tests/upgrade-rollback \
		--without-flags-in-bootloader

arm-%.o: %.c
	$(CROSS_COMPILE)$(CC) $(CFLAGS) $(CFLAGS_SA) $(filter %.c,$^) -c -o $@

arm-flags.o: ecc_tables.h

flags.o: ecc_tables.h

gentable: gentable.c
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@

ecc_tables.h: gentable
	./gentable > $@

tests/helper: tests/helper.c libbootflags.a
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@ -I. -L. -lbootflags

tester: tester.c fldebug.c libbootflags.a
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@ -L. -lbootflags

ecc-tester: ecc-tester.c libbootflags.a
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@ -L. -lbootflags

flagtool: flagtool.c fldebug.c libbootflags.a
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@ -L. -lbootflags

libbootflags-uboot.a: arm-flags.o
	$(AR) cr libbootflags-uboot.a $^

libbootflags.a: flags.o
	$(AR) cr libbootflags.a $^

bootflags: arm-main.o arm-bootflags.o arm-c_support.o libbootflags-uboot.a
	$(CROSS_COMPILE)$(LD) -g -Ttext 0x10800000 -o $@ -e main $^ $(PLATFORM_LIBGCC) -lgcc -L. -lbootflags-uboot

flag.blob: flagtool
	./flagtool -F 1 -p flag.blob

ifeq ($(ARCH),armv7l)
install: flagtool hooks/flagtool scripts/flagcheck libbootflags.a libbootflags-uboot.a
else
install: flagtool hooks/flagtool scripts/flagcheck libbootflags.a
endif
	mkdir -p $(HOOKDIR)
	$(INSTALL) hooks/flagtool $(HOOKDIR)
	mkdir -p $(SCRIPTDIR)/local-premount
	$(INSTALL) scripts/flagcheck $(SCRIPTDIR)/local-premount
	mkdir -p $(SYSTEMDDIR)/user/chaiwala.target.wants
	$(INSTALL) systemd/flagtool.service $(SYSTEMDDIR)/user
	ln -sf ../flagtool.service $(SYSTEMDDIR)/user/chaiwala.target.wants/
	mkdir -p $(LIBDIR)
	$(INSTALL) libbootflags.a $(LIBDIR)
	mkdir -p $(INCLUDEDIR)
	$(INSTALL) bootflags.h $(INCLUDEDIR)
	mkdir -p $(TOOLDIR)
	$(INSTALL) flagtool $(TOOLDIR)
	$(INSTALL) scripts/apertis-dev-update-factory-reset $(TOOLDIR)
ifeq ($(ARCH),armv7l)
	$(INSTALL) libbootflags-uboot.a $(LIBDIR)
endif

clean:
	rm -f *.o tester flagtool ecc_tables.h gentable \
	      ecc-tester *.a flag.blob

/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ecc_tables.h"

int test_encode_decode(int debug)
{
	unsigned char tempc;
	unsigned short temps;
	int i;

	for (i = 0; i < 16; i++) {
		tempc = ecc_enc_table[i];
		temps = ecc_dec_table[tempc];
		if (debug)
			printf("%d - %d : %d\n", i, tempc, temps);
		if (i != temps) {
			printf("Test code %d encodes to %d.\n", i, tempc);
			printf("%d does not decode to %d, instead decodes to %d\n", tempc, i, temps);
			return 1;
		}
	}
	return 0;
}

int test_one_bit_error(int debug)
{
	unsigned char tempc;
	unsigned short temps;
	int i, j, errcode;

	for (i = 0; i < 16; i++) {
		tempc = ecc_enc_table[i];
		for (j = 0; j < 8; j++) {
			errcode = tempc ^ (1 << j);
			temps = ecc_dec_table[errcode];
			if (debug)
				printf("%d - %d (flip bit %d) %d : %d\n", i, tempc, j, errcode, temps);
			if (i != temps) {
				printf("Test code %d encodes to %d\n", i, tempc);
				printf("Damaged code %d is %d with a single bit error.\n", errcode, tempc);
				printf("%d does not decode to %d, instead decodes to %d\n", errcode, i, temps);
				return 1;
			}
		}
	}
	return 0;
}

int test_two_bit_error(int debug)
{
	unsigned char tempc;
	unsigned short temps;
	int i, j, k, errcode;

	for (i = 0; i < 16; i++) {
		tempc = ecc_enc_table[i];
		for (j = 0; j < 8; j++)
			for (k = 0; k < 8; k++) {
				if (k == j)
					continue;
				errcode = tempc ^ (1 << j) ^ (1 << k);
				temps = ecc_dec_table[errcode];
				if (debug)
					printf("%d - %d (flip bits %d, %d) %d : %d\n", i, tempc, j, k, errcode, temps);
				if (temps != 65535) {
					printf("Test code %d encodes to %d\n", i, tempc);
					printf("Damaged code %d is %d with a two bit(%d, %d) error.\n", errcode, tempc, j, k);
					printf("%d does not decode to 65535 (the error code), instead it decodes to %d\n", errcode, temps);
					return 1;
				}
		}
	}
	return 0;
}

int nibble_preserved(int debug)
{
	int i;
	unsigned char tempc;

	for (i = 0; i < 16; i++) {
		tempc = ecc_enc_table[i];
		if ((tempc & 0xF) != i) {
			printf("Low nibble of output code 0x%x does not equal input code 0x%x\n", tempc, i);
			return 1;
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	if (test_encode_decode(0)) {
		printf("ECC code fails to decode error free codes\n");
		return EXIT_FAILURE;
	}
	if (test_one_bit_error(0)) {
		printf("ECC code fails to decode codes with single bit errors\n");
		return EXIT_FAILURE;
	}
	if (test_two_bit_error(0)) {
		printf("ECC code fails to notice two bit errors\n");
		return EXIT_FAILURE;
	}
	if (nibble_preserved(0)) {
		printf("ECC code fails nibble preservation test\n");
		return EXIT_FAILURE;
	}
	printf("ECC code test successful\n");
	return EXIT_SUCCESS;
}

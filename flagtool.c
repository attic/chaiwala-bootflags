/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include "bootflags.h"
#include "fldebug.h"

#define N_ELEMENTS(arr) (sizeof (arr) / sizeof ((arr)[0]))

#define FLAGS_PARTITION_PRIMARY "/dev/disk/by-partlabel/flags"
#define FLAGS_PARTITION_BACKUP "/dev/disk/by-partlabel/flags_backup"

/* update-pre reboot */
/*	|<- version          ->|<- flags ->|<- Hamming code (ECC) */
unsigned char update_flags[FLAGS_SIZE] = {
	/* minimal boot 1, v1, IN_USE|RUNNING|FACTORY */
	0x01, 0x00, 0x00, 0x00, 0x51, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x1a, 0xff,
	/* minimal boot 2, v2, IN_USE|PREFERRED|CLEAN */
	0x02, 0x00, 0x00, 0x00, 0x83, 0x00, 0x2f, 0xff, 0xff, 0xff, 0xc8, 0xff,
	/* full user-space 1, v1, IN_USE|RUNNING */
	0x01, 0x00, 0x00, 0x00, 0x11, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x11, 0xff,
	/* full user-space 2, v2, IN_USE|PREFERRED|CLEAN */
	0x02, 0x00, 0x00, 0x00, 0x83, 0x00, 0x2f, 0xff, 0xff, 0xff, 0xc8, 0xff,
};

/* update-poste reboot */
unsigned char update_booted_flags[FLAGS_SIZE] = {
	/* v1, IN_USE|FACTORY */
	0x01, 0x00, 0x00, 0x00, 0x41, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x14, 0xff,
	/* v2, IN_USE|PREFERRED|RUNNING */
	0x02, 0x00, 0x00, 0x00, 0x13, 0x00, 0x2f, 0xff, 0xff, 0xff, 0xc1, 0xff,
	/* v1, IN_USE */
	0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x1f, 0xff,
	/* v2, IN_USE|PREFERRED|RUNNING */
	0x02, 0x00, 0x00, 0x00, 0x13, 0x00, 0x2f, 0xff, 0xff, 0xff, 0xc1, 0xff
};

/* post rollback */
unsigned char rollback_flags[FLAGS_SIZE] = {
	/* v1, IN_USE|RUNNING|FACTORY */
	0x01, 0x00, 0x00, 0x00, 0x51, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x1a, 0xff,
	/* v2, IN_USE|PREFERRED */
	0x02, 0x00, 0x00, 0x00, 0x03, 0x00, 0x2f, 0xff, 0xff, 0xff, 0xcf, 0xff,
	/* v1, IN_USE|RUNNING */
	0x01, 0x00, 0x00, 0x00, 0x11, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x11, 0xff,
	/* v2, IN_USE|PREFERRED|FAILED */
	0x02, 0x00, 0x00, 0x00, 0x07, 0x00, 0x2f, 0xff, 0xff, 0xff, 0x7f, 0xff
};

/* post blacklist */
unsigned char blacklist_flags[FLAGS_SIZE] = {
	/* v1, IN_USE|RUNNING|FACTORY */
	0x01, 0x00, 0x00, 0x00, 0x51, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x1a, 0xff,
	/* v3, IN_USE|PREFERRED|FAILED */
	0x03, 0x00, 0x00, 0x00, 0x07, 0x01, 0xcf, 0xff, 0xff, 0xff, 0x7f, 0x1f,
	/* v1, IN_USE|RUNNING */
	0x01, 0x00, 0x00, 0x00, 0x11, 0x00, 0x1f, 0xff, 0xff, 0xff, 0x11, 0xff,
	/* v1, IN_USE|PREFERRED|FAILED */
	0x03, 0x00, 0x00, 0x00, 0x07, 0x01, 0xcf, 0xff, 0xff, 0xff, 0x7f, 0x1f
};

struct testname {
	char *name;
	unsigned char *flags;
};

struct testname names[] = {
	{
		.name = "update",
		.flags = update_flags
	},
	{
		.name = "update_booted",
		.flags = update_booted_flags
	},
	{
		.name = "rollback",
		.flags = rollback_flags
	},
	{
		.name = "blacklist",
		.flags = blacklist_flags
	},
	{NULL, NULL}
};

static int read_flags(char *filename, struct flag_entry *f)
{
	int fd, count = 0;
	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return fd;

	count = read(fd, f, FLAGS_SIZE);
	close(fd);
	if (count < FLAGS_SIZE)
		return 1;

	return 0;
}

static int write_flags(char *filename, struct flag_entry *f)
{
	int fd, count = 0;

	fd = open(filename, O_WRONLY|O_CREAT, 0644);
	if (fd < 0)
		return fd;
	count = write(fd, f, FLAGS_SIZE);
	close(fd);
	if (count != FLAGS_SIZE)
		return 1;
	return 0;
}

static void print_help(void)
{
	printf("flagtool - a tool for managing boot flags.\n");
	printf("Options:\n");
	printf("-F              :   set flags to factory state\n");
	printf("-l		:   force limited functionality next boot\n");
	printf("-s		:   force service request next boot\n");
	printf("-r		:   force factory reset on next boot\n");
	printf("-L              :   act as bootloader\n");
	printf("-u              :   act as mini userspace\n");
	printf("-U              :   act as full userspace\n");
	printf("-p <filename>   :   primary flag location\n");
	printf("-p /            :   shorthand for -p %s\n", FLAGS_PARTITION_PRIMARY);
	printf("-b <filename>   :   backup flag location\n");
	printf("-b /            :   shorthand for -b %s\n", FLAGS_PARTITION_BACKUP);
	printf("-d              :   dump flags\n");
	printf("-n              :   do nothing\n");
	printf("-f              :   previous step failed\n");
	printf("-B		:   blacklist current system\n");
	printf("-T <string>	:   test mode (compare flags to named states)\n");
}

static int perform_factory_reset(char *primary, char *secondary, int version,
                                 int fake, int fail_all)
{
	struct flag_entry flags[4];
	int err;
	flaglib_factory_flags(flags, version);

	if (fail_all)
		flaglib_fail_all(flags);

	if (primary) {
		err = write_flags(primary, flags);
		if (err)
			return 1;
	}

	if (secondary) {
		err = write_flags(secondary, flags);
		if (err)
			return 1;
	}

	if (!secondary && !primary) {
		int count;
		count = write(STDOUT_FILENO, flags, sizeof(flags));
		if (count != sizeof(flags))
			return 1;
	}
	return 0;
}

static unsigned char *name_to_flags(char *name)
{
	int i;

	for (i = 0; names[i].name != NULL; i++)
		if (strcmp(names[i].name, name) == 0)
			return names[i].flags;

	printf("Illegal test string, valid values are:\n");

	for (i = 0; names[i].name != NULL; i++)
		printf("%s\n", names[i].name);

	exit(EXIT_FAILURE);
}

static void
ensure_flag_locations(char **primary, char **backup)
{
	if (primary != NULL && *primary == NULL)
		*primary = strdup(FLAGS_PARTITION_PRIMARY);
	if (backup != NULL && *backup == NULL)
		*backup = strdup(FLAGS_PARTITION_BACKUP);
}

int main(int argc, char **argv)
{
	int opt, factory = 0, dumpflags = 0, donothing = 0, limited = 0, reset = 0;
	int service = 0;
	unsigned char *test = NULL;
	enum flaglib_mode mode = FLAGLIB_UNSET;
	enum flaglib_step next;
	enum flaglib_status status = FLAGLIB_SUCCEED;
	int factory_version = 0;
	char *primary = NULL, *backup = NULL;
	struct flag_entry flags[4];
	int err;
	int flagset;

	while ((opt = getopt(argc, argv, "T:fF:uUdp:b:nBlsrL")) != -1) {
		switch (opt) {
		case 'l':
			limited = 1;
			break;
		case 's':
			service = 1;
			break;
		case 'r':
			reset = 1;
			break;
		case 'T':
			test = name_to_flags(optarg);
			break;
		case 'f':
			if (status != FLAGLIB_SUCCEED) {
				printf("Only one of -f or -B may be used at a time\n");
				exit(EXIT_FAILURE);
			}
			status = FLAGLIB_FAIL;
			break;
		case 'F':
			factory = 1;
			factory_version = atoi(optarg);
			break;
		case 'L':
			if (mode) {
				printf("Only one of -L, -u or -U may be used at a time\n");
				exit(EXIT_FAILURE);
			}
			mode = FLAGLIB_BOOTLOADER;
			break;
		case 'u':
			if (mode)  {
				printf("Only one of -L, -u or -U may be used at a time\n");
				exit(EXIT_FAILURE);
			}
			mode = FLAGLIB_MINI_USERSPACE;
			break;
		case 'U':
			if (mode)  {
				printf("Only one of -L, -u or -U may be used at a time\n");
				exit(EXIT_FAILURE);
			}
			mode = FLAGLIB_FULL_USERSPACE;
			break;
		case 'd':
			dumpflags = 1;
			break;
		case 'p':
			if (strcmp(optarg, "/") == 0)
				primary = strdup(FLAGS_PARTITION_PRIMARY);
			else
				primary = strdup(optarg);
			break;
		case 'b':
			if (strcmp(optarg, "/") == 0)
				backup = strdup(FLAGS_PARTITION_BACKUP);
			else
				backup = strdup(optarg);
			break;
		case 'n':
			donothing = 1;
			break;
		case 'B':
			if (status != FLAGLIB_SUCCEED) {
				printf("Only one of -f or -B may be used at a time\n");
				exit(EXIT_FAILURE);
			}
			status = FLAGLIB_BLACKLIST;
			break;
		default:
			print_help();
			exit(EXIT_FAILURE);
		}
	}
	if (test) {
		struct flag_entry flags_backup[4];
		struct flag_entry without_transient[4];
		unsigned int i;

		ensure_flag_locations(&primary, &backup);

		memcpy(without_transient, test, FLAGS_SIZE);
		for (i = 0; i < N_ELEMENTS(without_transient); i++) {
			/* documented to not be in the backup by the design
			 * document, to reduce flash wear a bit */
			without_transient[i].flags &= ~(FLAG_STARTING|FLAG_RUNNING);
		}
		flaglib_set_ecc_for_testing(without_transient);

		if (dumpflags) {
			struct flag_entry tmp[4];

			memcpy(tmp, test, FLAGS_SIZE);
			fldebug_flag_dump("expected flags", tmp);

			if (memcmp(test, without_transient, FLAGS_SIZE) != 0) {
				fldebug_flag_dump("expected flags, with STARTING|RUNNING masked out", without_transient);
			}
		}

		err = read_flags(primary, flags);
		if (err) {
			printf("FAIL : Failed to read flags\n");
			exit(EXIT_FAILURE);
		}
		if (dumpflags) {
			if (memcmp(test, flags, FLAGS_SIZE) == 0) {
				fprintf(stderr, "primary flags match expected flags\n");
			} else {
				fldebug_flag_dump("primary flags", flags);
			}
		}

		err = read_flags(backup, flags_backup);
		if (err) {
			fprintf(stderr, "FAIL : Failed to read backup flags\n");
			exit(EXIT_FAILURE);
		}
		if (dumpflags) {
			if (memcmp(flags, flags_backup, FLAGS_SIZE) == 0) {
				fprintf(stderr, "backup flags match primary flags\n");
			} else if (memcmp(test, flags_backup, FLAGS_SIZE) == 0) {
				fprintf(stderr, "backup flags match expected primary flags\n");
			} else if (memcmp(without_transient, flags_backup, FLAGS_SIZE) == 0) {
				fprintf(stderr, "backup flags match expected flags with STARTING|RUNNING masked out\n");
			} else {
				fldebug_flag_dump("backup flags", flags_backup);
			}
		}

		if (memcmp(test, flags, FLAGS_SIZE) != 0) {
			printf("FAIL : flags don't match expected state\n");
			exit(EXIT_FAILURE);
		}

		if (memcmp(test, flags_backup, FLAGS_SIZE) != 0 &&
				memcmp(without_transient, flags_backup, FLAGS_SIZE) != 0) {
			printf("FAIL : backup flags don't match either of the expected states\n");
			exit(EXIT_FAILURE);
		}
		printf("PASS\n");
		exit(EXIT_SUCCESS);
	}
	if (limited) {
		perform_factory_reset(primary, backup, 1, donothing, 0);
		flaglib_set_mode(FLAGLIB_BOOTLOADER);
		read_flags(primary, flags);
		flaglib_next_step(flags, FLAGLIB_SUCCEED);
		flaglib_set_mode(FLAGLIB_MINI_USERSPACE);
		flaglib_next_step(flags, FLAGLIB_SUCCEED);
		flaglib_set_mode(FLAGLIB_FULL_USERSPACE);
		flaglib_next_step(flags, FLAGLIB_FAIL);
		write_flags(primary, flags);
		exit(EXIT_SUCCESS);
	}

	if (service) {
		perform_factory_reset(primary, backup, 1, donothing, 1);
		exit(EXIT_SUCCESS);
	}

	if (reset) {
		read_flags(primary, flags);
		flaglib_force_factory_reset(flags);
		write_flags(primary, flags);
		exit(EXIT_SUCCESS);
	}

	if (factory) {
		int ret;
		if (!factory_version)
			exit(EXIT_FAILURE);
		ret = perform_factory_reset(primary, backup, factory_version,
		                            donothing, 0);
		if (ret)
			exit(EXIT_FAILURE);
		else
			exit(EXIT_SUCCESS);
	}
	if (mode) {
		flagset = 1;
		err = read_flags(primary, flags);
		if (err && backup) {
			flagset = 2;
			err = read_flags(backup, flags);
		}
		if (err) {
			printf("Flag read failure\n");
			exit(err);
		}
#ifndef __arm__
/* since we're not on ARM, the bootloader can't have done any set up
 * so we run through the bootloader steps to set up the flags properly
*/
		if (mode == FLAGLIB_MINI_USERSPACE) {
			printf("Performing boot loader steps\n");
			flaglib_set_mode(FLAGLIB_BOOTLOADER);
			while (1) {
				if (dumpflags)
					fldebug_flag_dump("flags", flags);
				next = flaglib_next_step(flags, status);
				printf("%s\n", flaglib_step_as_string(next));
				if ((next == FLAGLIB_ECC_FAIL)
				 || (next == FLAGLIB_FLAGS_INCONSISTENT)) {
					printf("Bad flags\n");
					if (flagset == 2 || !backup)
						exit(next);
					flagset = 2;
					err = read_flags(backup, flags);
					continue;
				}
				break;
			}
		}
#endif

		flaglib_set_mode(mode);
		if (dumpflags)
			fldebug_flag_dump("flags", flags);
		next = flaglib_next_step(flags, status);
		if (dumpflags)
			fldebug_flag_dump("flags", flags);
		if (primary && !donothing) {
			err = write_flags(primary, flags);
			if (err)
				exit(FLAGLIB_FLAGS_INCONSISTENT);
		}
		exit(next);
	}
	if (dumpflags) {
		struct flag_entry flags_backup[4];

		ensure_flag_locations(&primary, &backup);

		err = read_flags(primary, flags);
		if (err) {
			fprintf(stderr, "FAIL : Failed to read flags\n");
			exit(EXIT_FAILURE);
		}
		fldebug_flag_dump("primary flags", flags);
		/* flagtool -d -b "" only reads the primary partition */
		if (backup[0]) {
			err = read_flags(backup, flags_backup);
			if (err) {
				fprintf(stderr, "FAIL : Failed to read flags_backup\n");
				exit(EXIT_FAILURE);
			}
			if (memcmp (flags, flags_backup, sizeof(flags)) == 0) {
				fprintf (stderr, "backup flags match primary flags\n");
			}
			else {
				fldebug_flag_dump("backup flags (not matching primary)", flags_backup);
			}
		}
		exit(EXIT_SUCCESS);
	}
	exit(EXIT_SUCCESS);
}

/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdint.h>
#include "bootflags.h"
#include "fldebug.h"

void fldebug_flag_dump(const char *title, struct flag_entry *f)
{
	int i, j, r;

	fprintf(stderr, "=== Dumping %s ===\n", title);
	for (i = 0; i < 4; i++) {
		const unsigned char *bytes = (const unsigned char *) &f[i];

		fprintf(stderr, "Entry %d\n", i);

		/* This matches the format of
		 *     sudo xxd -l48 -c12 /dev/disk/by-partlabel/flags
		 * and can be pasted into the stdin of
		 *     sudo xxd -l48 -c12 -r - /dev/disk/by-partlabel/flags
		 * if you want to binary-patch your flags to a known state.
		 */

		fprintf(stderr, "%08x:", (int) sizeof(*f) * i);
		for (j = 0; j < sizeof(*f); j += 2) {
			fprintf(stderr, " %02x%02x", bytes[j], bytes[j+1]);
		}
		fprintf(stderr, "\n");

		fprintf(stderr, "  ECC: ");
		r = flaglib_check_ecc(&f[i], 1);
		if (r < 0)
			fprintf(stderr, "uncorrectable\n");
		else if (r)
			fprintf(stderr, "corrected %d bit-errors\n", r);
		else
			fprintf(stderr, "all valid\n");
		if (r) {
			fprintf(stderr, "%08x:", (int) sizeof(*f) * i);
			for (j = 0; j < sizeof(*f); j += 2) {
				fprintf(stderr, " %02x%02x", bytes[j], bytes[j+1]);
			}
			fprintf(stderr, "\n");
		}

		fprintf(stderr, "  Version: %d\n", f[i].mini_version);
		fprintf(stderr, "  Flags: ");
		if (f[i].flags & FLAG_IN_USE)
			fprintf(stderr, "IN_USE ");
		if (f[i].flags & FLAG_PREFERRED)
			fprintf(stderr, "PREFERRED ");
		if (f[i].flags & FLAG_FAILED)
			fprintf(stderr, "FAILED ");
		if (f[i].flags & FLAG_BLACKLISTED)
			fprintf(stderr, "BLACKLISTED ");
		if (f[i].flags & FLAG_STARTING)
			fprintf(stderr, "STARTING ");
		if (f[i].flags & FLAG_RUNNING)
			fprintf(stderr, "RUNNING ");
		if (f[i].flags & FLAG_UPDATING)
			fprintf(stderr, "UPDATING ");
		if (f[i].flags & FLAG_FACTORY)
			fprintf(stderr, "FACTORY ");
		if (f[i].flags & FLAG_CLEAN)
			fprintf(stderr, "CLEAN ");
		for (j = LOG2_NEXT_FLAG; j < 8 * sizeof(f[i].flags); j++) {
			if (f[i].flags & (1 << j))
				fprintf(stderr, "BIT%d ", j);
		}
		fprintf(stderr, "\n");
	}

	fprintf(stderr, "Sanity check: %s\n", flaglib_sanity_check(f) == 0 ? "valid" : "invalid");
	fprintf(stderr, "Viable for boot: %s\n", flaglib_boot_viable(f) ? "yes" : "no");
	fprintf(stderr, "=====================\n");
}

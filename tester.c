/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "bootflags.h"
#include "fldebug.h"

static int verbose = 0;

static struct flag_entry factory[4] = {
	{
		.mini_version = 1,
		.flags = FLAG_IN_USE | FLAG_PREFERRED | FLAG_FACTORY
	},
	{
		.flags = FLAG_FACTORY
	},
	{
		.mini_version = 1,
		.flags = FLAG_IN_USE | FLAG_PREFERRED | FLAG_FACTORY
	},
	{
		.flags = FLAG_FACTORY
	},
};

static struct flag_entry start[][4] = {
	{
		{
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		}
	},
	{
		{
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		}
	},
	{
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		}
	},
	{
		{
			.mini_version = 123,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 123,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		}
	},
	{
		{
			.mini_version = 123,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		},
		{
			.mini_version = 17,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 123,
			.flags = FLAG_IN_USE
		},
		{
			.mini_version = 1,
			.flags = FLAG_PREFERRED | FLAG_IN_USE
		}
	},
};

char failarray[] = {0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -1};
char failarray2[] = {0, 0, 1, 0, 0, 0, 2, -1};
char failarray3[] = {1, 0, 2, 0, 2, -1};

struct testinput {
	struct flag_entry *start;
	char *fails;
	int steps;
	enum flaglib_step final;
};


static void print_mode(enum flaglib_mode m)
{
	switch (m) {
	case FLAGLIB_UNSET:
		printf("mode: UNSET\n");
		break;
	case FLAGLIB_BOOTLOADER:
		printf("mode: BOOTLOADER\n");
		break;
	case FLAGLIB_MINI_USERSPACE:
		printf("mode: MINI_USERSPACE\n");
		break;
	case FLAGLIB_FULL_USERSPACE:
		printf("mode: FULL_USERSPACE\n");
		break;
	default:
		printf("mode: nonsense - %d\n", m);
	}
}

enum flaglib_mode next_mode(struct flag_entry *f, enum flaglib_step s)
{
	switch (s) {
	case FLAGLIB_RETURN_TO_MINI:
	case FLAGLIB_BOOT_PARTITION_1:
	case FLAGLIB_BOOT_PARTITION_2:
		return FLAGLIB_MINI_USERSPACE;
	case FLAGLIB_CONTINUE:
	case FLAGLIB_MOUNT_SUBVOLUME_1:
	case FLAGLIB_MOUNT_SUBVOLUME_2:
		return FLAGLIB_FULL_USERSPACE;
	case FLAGLIB_BOOT_FACTORY_RESET:
		memcpy(f, factory, sizeof(factory));
	case FLAGLIB_REBOOT:
		return FLAGLIB_BOOTLOADER;
	case FLAGLIB_LIMITED_FUNCTIONALITY:
	case FLAGLIB_REQUEST_SERVICING:
	case FLAGLIB_ECC_FAIL:
	case FLAGLIB_FLAGS_INCONSISTENT:
		return -1;
	}
	return FLAGLIB_UNSET;
}

int test(struct testinput *t)
{
	static struct flag_entry flags[4];
	int x = 0, count = 0;
	enum flaglib_step next;
	enum flaglib_mode mode = FLAGLIB_BOOTLOADER;
	int fail;

	memcpy(flags, t->start, sizeof(flags));
	do {
		count++;
		fail = 0;
		if (t->fails) {
			if (t->fails[x] == -1) {
				fail = 0;
			} else fail = t->fails[x++];
		}

		if (fail && verbose)
			printf("injecting a failure condition\n");

		if (fail == 2) {
			if (verbose)
				printf("injecting watchdog timeout\n");
			mode = FLAGLIB_BOOTLOADER;
		}

		flaglib_set_mode(mode);
		if (verbose)
			print_mode(mode);

		next = flaglib_next_step(flags, fail);
		if (verbose)
			printf("step %d: %s\n", count, flaglib_step_as_string(next));

		mode = next_mode(flags, next);
		if (count == t->steps) {
			if (verbose)
				printf("test finished\n");
			break;
		}
	} while (mode != -1);
	if (next == t->final)
		return 1;
	if (verbose)
		fldebug_flag_dump("flags", flags);
	return 0;
}

int main(int argc, char **argv)
{
	int i = 0;
	struct testinput tests[] = {
		{// complicated string of failures
			.start = start[1],
			.fails = failarray,
			.steps = 15,
			.final = FLAGLIB_LIMITED_FUNCTIONALITY
		},
		{// simple successful boot
			.start = start[1],
			.fails = NULL,
			.steps = 3,
			.final = FLAGLIB_CONTINUE
		},
		{//ecc failure
			.start = start[0],
			.fails = NULL,
			.steps = 1,
			.final = FLAGLIB_ECC_FAIL
		},
		{//inconsistent flags
			.start = start[2],
			.fails = NULL,
			.steps = 1,
			.final = FLAGLIB_FLAGS_INCONSISTENT
		},
		{//Fallback boot
			.start = start[3],
			.fails = NULL,
			.steps = 3,
			.final = FLAGLIB_CONTINUE
		},
		{//Factory fallback then watchdog trigger
			.start = start[4],
			.fails = failarray2,
			.steps = 7,
			.final = FLAGLIB_REQUEST_SERVICING
		},
		{//Multiple watchdog triggers
			.start = start[4],
			.fails = failarray3,
			.steps = 5,
			.final = FLAGLIB_REQUEST_SERVICING
		},
		{
			.start = NULL
		}
	};
	/* skip 0 for the ECC test */
	for (i = 1; i < sizeof(start)/FLAGS_SIZE; i++)
		flaglib_set_ecc_for_testing(start[i]);
	flaglib_set_ecc_for_testing(factory);

	i = 0;
	while (tests[i].start) {
		if (verbose)
			printf("TEST %d\n", i);
		if (!test(&tests[i++]))
			return EXIT_FAILURE;
	}
	printf("Flag state machine test successful\n");
	return EXIT_SUCCESS;
}

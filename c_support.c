/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
  An absolutely *filthy* hack to make just printf work

  u-boot makes an explicit exception for jumping through its jumptable
  so I *think* we can use this and still not be forced to be GPLv2

  However, this code is probably useless outside of debugging, so
  it probably shouldn't be linked into final software anyway...

  now dependent on setenv...
*/

#define offsetof(TYPE, MEMBER) (100)
#define XF_printf 5
#define XF_setenv 15

#define EXPORT_FUNC(x) \
	asm volatile (			\
"	.globl " #x "\n"		\
#x ":\n"				\
"	ldr	ip, [r8, %0]\n"		\
"	ldr	pc, [ip, %1]\n"		\
	: : "i"(offsetof(gd_t, jt)), "i"(XF_ ## x * sizeof(void *)) : "ip");

void __attribute__((unused)) dummy(void)
{
	EXPORT_FUNC(printf);
	EXPORT_FUNC(setenv);
}

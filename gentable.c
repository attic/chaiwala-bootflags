/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>

/* we xor all values with this so an all 0 block becomes invalid data
 *  due to the way this is used in flags.c, we must NEVER touch the low
 *  order nibble!
 */
#define PERTURBATION_CONSTANT 0xF0

unsigned int H[4][8] = {
{0, 1, 1, 1, 1, 0, 0, 0},
{1, 0, 1, 1, 0, 1, 0, 0},
{1, 1, 0, 1, 0, 0, 1, 0},
{1, 1, 1, 0, 0, 0, 0, 1}
};

unsigned int G[4][8] ={
{1, 0, 0, 0, 0, 1, 1, 1},
{0, 1, 0, 0, 1, 0, 1, 1},
{0, 0, 1, 0, 1, 1, 0, 1},
{0, 0, 0, 1, 1, 1, 1, 0}
};

int find_syndrome(unsigned int s[4])
{
	int i;
	for (i = 0; i < 8; i++)
		if (s[0] == H[0][i] && s[1] == H[1][i]
		    && s[2] == H[2][i] && s[3] == H[3][i])
			break;
	return i;
}

unsigned int dec(unsigned int code)
{
	unsigned int s[4] = {0, 0, 0, 0};
	int i, j;
//printf("code = 0x%x\n", code);
	code ^= PERTURBATION_CONSTANT;
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 4; j++)
			s[j] = (s[j] + H[j][i] * !!(code & (1 << i))) % 2;
		}
//	printf("Syndrome = %d %d %d %d\n", s[0], s[1], s[2], s[3]);
	if (s[0] + s[1] + s[2] + s[3]) {
		int f = find_syndrome(s);
		if (f == 8)
			return 0xFFFF;
		code ^= PERTURBATION_CONSTANT;
		return dec(code ^ (1 << f));
	}
	return code & 0xF;
}

unsigned int gen(unsigned int data)
{
	unsigned int code[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	unsigned int out;
	int i, j;

	for (j = 0; j < 4; j++)
		for (i = 0; i < 8; i++)
			code[i] = (code[i] + G[j][i] * !!(data & (1 << j))) % 2;

	for (i = 0, out = 0; i < 8; i++)
		out |= code[i] << i;
	return out ^ PERTURBATION_CONSTANT;
}

void dectable(void)
{
	int i;
	printf("static unsigned short ecc_dec_table[256] = {\n");
	for (i = 0; i < 256; i++) {
		printf("0x%x, ",  dec(i));
		if (!((i +1) % 8))
			printf("\n");
	}
	printf("};\n");
}
void enctable(void)
{
	int i;
	printf("static unsigned char ecc_enc_table[16] = {\n");
	for (i = 0; i < 16; i++) {
		printf("0x%x, ",  gen(i));
		if (!((i +1) % 8))
			printf("\n");
	}
	printf("};\n");
}


int main(int argc, char **argv)
{
	printf("#ifndef __ECC_TABLES__\n");
	printf("#define __ECC_TABLES__\n");
	enctable();
	dectable();
	printf("#endif /* __ECC_TABLES__ */\n");

	return 0;
}

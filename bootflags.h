/*
 * Copyright © 2012-2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FLAGS_H__
#define __FLAGS_H__

/* FLAGLIB_UNSET is 0 because it's used in boolean tests */
enum flaglib_mode {
	FLAGLIB_UNSET = 0,
	FLAGLIB_BOOTLOADER,
	FLAGLIB_MINI_USERSPACE,
	FLAGLIB_FULL_USERSPACE
};

enum flaglib_status {
	FLAGLIB_SUCCEED = 0,
	FLAGLIB_FAIL,
	FLAGLIB_BLACKLIST
};

enum flaglib_step {
	FLAGLIB_CONTINUE,
	FLAGLIB_BOOT_PARTITION,
	FLAGLIB_BOOT_PARTITION_1 = FLAGLIB_BOOT_PARTITION,
	FLAGLIB_BOOT_PARTITION_2,
	FLAGLIB_BOOT_FACTORY_RESET,
	FLAGLIB_MOUNT_SUBVOLUME,
	FLAGLIB_MOUNT_SUBVOLUME_1 = FLAGLIB_MOUNT_SUBVOLUME,
	FLAGLIB_MOUNT_SUBVOLUME_2,
	FLAGLIB_RETURN_TO_MINI,
	FLAGLIB_REBOOT,
	FLAGLIB_LIMITED_FUNCTIONALITY,
	FLAGLIB_REQUEST_SERVICING,
	FLAGLIB_ECC_FAIL,
	FLAGLIB_FLAGS_INCONSISTENT,
};

/*
	These are passed through dbus API, so don't renumber!
*/
enum flaglib_boot_type {
	FLAGLIB_BOOT_NORMAL,
	FLAGLIB_BOOT_SUCCESSFUL_UPDATE,
	FLAGLIB_BOOT_FAILED_UPDATE,
	FLAGLIB_BOOT_ROLLBACK
};

#define FLAG_IN_USE              (1 << 0)   /* 0x0001 */
#define FLAG_PREFERRED           (1 << 1)   /* 0x0002 */
#define FLAG_FAILED              (1 << 2)   /* 0x0004 */
#define FLAG_STARTING            (1 << 3)   /* 0x0008 */
#define FLAG_RUNNING             (1 << 4)   /* 0x0010 */
#define FLAG_UPDATING            (1 << 5)   /* 0x0020 */
#define FLAG_FACTORY             (1 << 6)   /* 0x0040 */
#define FLAG_CLEAN               (1 << 7)   /* 0x0080 */
#define FLAG_BLACKLISTED         (1 << 8)   /* 0x0100 */
#define FLAG_FROM_ROLLBACK       (1 << 9)   /* 0x0200 */
#define LOG2_NEXT_FLAG           10

/* should we put some statistics in this structure?
*/
#define FLAG_BYTES 6
struct __attribute__ ((__packed__)) flag_entry {
	int32_t mini_version;
	uint16_t flags;
	unsigned char ecc[FLAG_BYTES];
};
#define FLAGS_SIZE (sizeof(struct flag_entry) * 4)

void flaglib_set_mode(enum flaglib_mode m);

enum flaglib_step flaglib_next_step(struct flag_entry *in,
                                    enum flaglib_status s);

void flaglib_force_factory_reset(struct flag_entry *in);

void flaglib_set_ecc_for_testing(struct flag_entry *in);

int flaglib_begin_mini_update(struct flag_entry *in, uint32_t mini_version);

int flaglib_begin_full_update(struct flag_entry *in, uint32_t mini_version);

int flaglib_finish_update(struct flag_entry *in,
                          struct flag_entry *backup,
                          enum flaglib_status s);

void flaglib_factory_flags(struct flag_entry *in, uint32_t version);

const char *flaglib_boot_type_as_string(enum flaglib_boot_type type);

const char *flaglib_step_as_string(int step);

int flaglib_get_mini_version(struct flag_entry *in, int entry);

int flaglib_get_next_version(struct flag_entry *in);

int flaglib_clear_blacklist(struct flag_entry *in, int *mini_version);

enum flaglib_boot_type flaglib_get_boot_type(struct flag_entry *in);

void flaglib_fail_all(struct flag_entry *in);

int flaglib_check_ecc(struct flag_entry *in, unsigned int n_entries);

int flaglib_sanity_check(struct flag_entry *in);

int flaglib_boot_viable(struct flag_entry *in);

#endif /* __FLAGS_H__ */

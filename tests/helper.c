/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include "bootflags.h"

#define N_ELEMENTS(arr) (sizeof (arr) / sizeof (arr[0]))

static void
read_flags (const char *file,
    struct flag_entry flags[4])
{
  FILE *fh;

  fh = fopen (file, "rb");

  if (fh == NULL)
    {
      perror ("fopen");
      abort ();
    }

  /* We cannot use sizeof(arr) here as the size of 4 records,
   * or N_ELEMENTS(arr) as the record count, because
   * struct flag_entry flags[4] as a function argument is just syntactic
   * sugar for a pointer, so sizeof(flags) == sizeof(void *) and
   * sizeof(*flags) == sizeof(flags[0]) == sizeof(struct flag_entry). */
  if (fread (flags, sizeof (struct flag_entry), 4, fh) != 4)
    {
      fprintf (stderr, "fread failed or short read\n");
      abort ();
    }

  if (fclose (fh) != 0)
    {
      perror ("fclose");
      abort ();
    }
}

static void
write_flags (const char *file,
    const struct flag_entry flags[4])
{
  FILE *fh;

  fh = fopen (file, "wb");

  if (fh == NULL)
    {
      perror ("fopen");
      abort ();
    }

  /* See comment in read_flags() before doing anything clever with sizeof(). */
  if (fwrite (flags, sizeof (struct flag_entry), 4, fh) != 4)
    {
      fprintf (stderr, "fwrite failed or short write\n");
      abort ();
    }

  if (fclose (fh) != 0)
    {
      perror ("fclose");
      abort ();
    }
}

int
main (int argc,
    char **argv)
{
  struct flag_entry flags[4];
  struct flag_entry flags_backup[4];
  static const char *primary;
  static const char *backup;
  static const char *action;
  int part_id;
  int next;
  int next_version;

  if (argc != 4)
    {
      printf ("usage: %s flags flags_backup ACTION\n", argv[0]);
      return EX_USAGE;
    }

  primary = argv[1];
  backup = argv[2];
  action = argv[3];

  if (strcmp (action, "update-root") == 0)
    {
      /* Imitate what chaiwala-updaterd does before unpacking a new
       * root filesystem (full userspace) into the inactive subvolume.
       * The exit status is the boot partition (mini-userspace) that
       * was used, or EX_DATAERR if the update would have failed.
       * Reference: chaiwala-btrfs/0.5.4 begin_fu_upgrade_get_part_id() */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      part_id = flaglib_begin_full_update (flags, 0);

      if (part_id < 0)
        return EX_DATAERR;

      write_flags (primary, flags);
      printf ("partition selected for update: %d\n", part_id);
      return part_id;
    }
  else if (strcmp (action, "update-mini") == 0)
    {
      /* Imitate what chaiwala-updaterd does before unpacking a new
       * kernel and initramfs (mini-userspace) into the inactive
       * boot partition.
       * The exit status is the boot partition (mini-userspace) that
       * was used, or EX_DATAERR if the update would have failed.
       * Reference: chaiwala-btrfs/0.5.4 begin_mu_upgrade_get_part_id() */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      next_version = flaglib_get_next_version (flags);
      part_id = flaglib_begin_full_update (flags, next_version);

      if (part_id < 0)
        return EX_DATAERR;

      part_id = flaglib_begin_mini_update (flags, next_version);

      if (part_id < 0)
        return EX_DATAERR;

      printf ("partition selected for update: %d\n", part_id);
      write_flags (primary, flags);
      return part_id;
    }
  else if (strcmp (action, "finish-update") == 0)
    {
      /* Imitate what chaiwala-updaterd does at the end of a successful
       * upgrade.
       * The exit status is the new mini-userspace version number.
       * Reference: chaiwala-btrfs/0.5.4 update_flags_on_upgrade_success() */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      read_flags (backup, flags_backup);
      next_version = flaglib_finish_update (flags, flags_backup,
          FLAGLIB_SUCCEED);
      write_flags (primary, flags);
      write_flags (backup, flags_backup);
      printf ("next version: %d\n", next_version);
      return next_version;
    }
  else if (strcmp (action, "end-of-boot") == 0)
    {
      /* Imitate what chaiwala-updaterd does at the end of a successful
       * boot.
       * The exit status is the next step to take, typically FLAGLIB_CONTINUE.
       * Reference: chaiwala-btrfs/0.5.4 handle_set_boot_finished() */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      next = flaglib_next_step (flags, FLAGLIB_SUCCEED);
      write_flags (primary, flags);
      /* yes this is deliberate: we are writing the same flags to the primary
       * and backup records */
      write_flags (backup, flags);
      printf ("next step: %d\n", next);
      return next;
    }
  else if (strcmp (action, "rollback") == 0)
    {
      /* Imitate what chaiwala-rollbackd does in order to roll back a
       * bad upgrade.
       * The exit status is the next step to take, typically
       * FLAGLIB_RETURN_TO_MINI.
       * Reference: chaiwala-rollbackd/0.1.0co4 rollbackd_rollback()
       */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      next = flaglib_next_step (flags, FLAGLIB_FAIL);
      write_flags (primary, flags);
      printf ("next step: %d\n", next);
      return next;
    }
  else if (strcmp (action, "blacklist") == 0)
    {
      /* Imitate what chaiwala-rollbackd does when asked to blacklist a
       * version.
       * The exit status is the next step to take.
       * Reference: chaiwala-rollbackd/0.1.0co4 rollbackd_blacklist()
       */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      next = flaglib_next_step (flags, FLAGLIB_BLACKLIST);
      write_flags (primary, flags);
      printf ("next step: %d\n", next);
      return next;
    }
  else if (strcmp (action, "factory-reset") == 0)
    {
      /* Imitate what chaiwala-rollbackd does when asked to perform a
       * factory reset.
       * The exit status is 0 (because flaglib_force_factory_reset()
       * does not return anything).
       * Reference: chaiwala-rollbackd/0.1.0co4 rollbackd_factory_reset()
       */
      flaglib_set_mode (FLAGLIB_FULL_USERSPACE);
      read_flags (primary, flags);
      flaglib_force_factory_reset (flags);
      write_flags (primary, flags);
      return 0;
    }
  else if (strcmp (action, "ecc") == 0)
    {
      int p, b;

      /* Exit with 0 if the ECCs are valid, a small integer if bit-errors were
       * corrected, or EX_DATAERR if any were uncorrectable. */
      read_flags (primary, flags);
      p = flaglib_check_ecc (flags, N_ELEMENTS (flags));

      if (p < 0)
        return EX_DATAERR;

      read_flags (backup, flags_backup);
      b = flaglib_check_ecc (flags_backup, N_ELEMENTS (flags_backup));

      if (b < 0)
        return EX_DATAERR;

      return p + b;
    }
  else if (strcmp (action, "set-ecc") == 0)
    {
      /* Assume that the primary partition's data is correct and the ECCs
       * are wrong. Set the ECCs to match the data. */
      read_flags (primary, flags);
      flaglib_set_ecc_for_testing (flags);
      write_flags (primary, flags);
      return 0;
    }
  else if (strcmp (action, "sanity") == 0)
    {
      int r = 0;

      /* Exit 0 if the sanity-check passes, 1 if the primary flags fail
       * the sanity check, 2 if the backup flags fail, 3 if both. */
      read_flags (primary, flags);

      if (flaglib_sanity_check (flags) != 0)
        r |= 1;

      read_flags (backup, flags_backup);

      if (flaglib_sanity_check (flags_backup) != 0)
        r |= 2;

      return r;
    }
  else if (strcmp (action, "viable") == 0)
    {
      int r = 0;

      /* Exit 0 if boot is viable, 1 if the primary flags are
       * non-viable, 2 if the backup flags are non-viable, 3 if both are
       * non-viable. */
      read_flags (primary, flags);

      if (flaglib_sanity_check (flags) != 0)
        r |= 1;

      read_flags (backup, flags_backup);

      if (flaglib_sanity_check (flags_backup) != 0)
        r |= 2;

      return r;
    }
  else
    {
      fprintf (stderr, "unknown action\n");
      return EX_USAGE;
    }
}

/* vim: set sts=2 sw=2 et: */

